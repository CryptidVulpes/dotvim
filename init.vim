" ======================================================================
" Here begins awesomeness
" ======================================================================
" Introducing the new and the shiny ... Vundle!
set nocompatible
filetype off
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()
" === Install new plugins here === "
Plugin 'FelikZ/ctrlp-py-matcher'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'andviro/flake8-vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'fisadev/vim-isort'
Plugin 'VundleVim/Vundle.vim'
Plugin 'hynek/vim-python-pep8-indent'
Plugin 'kien/ctrlp.vim'
Plugin 'newclear/vim-pyclewn'
Plugin 'tomtom/tlib_vim'
Plugin 'vim-scripts/Zenburn'
Plugin 'tpope/vim-fugitive'
Plugin 'xolox/vim-easytags'
Plugin 'xolox/vim-misc'
Plugin 'Valloric/YouCompleteMe'
Plugin 'tweekmonster/gofmt.vim'
" === End installing plugins === "
call vundle#end()            " required
filetype plugin indent on    " required

" === Look & Feel ===
set hidden
set scrolloff=5  " when to scroll down
set number          " line numbers
set ruler         " cursor position
set showcmd         " show visual box sizes
set viminfo='0,:0,<0,@0,f0
set noshowmode
set noerrorbells
set novisualbell
set t_vb=
" 0: never
" 1: only if at least two windows
" 2: always
set laststatus=2 " show statusline always (more fun that way)
"""" tabs and mousecontrol
set switchbuf=usetab,newtab
set mouse=a

" === Custom Highlights ===
set hlsearch    " Hilight searches
syntax on
syntax match SpacesBeforeTabs / \+\ze\t/
syntax match Tabs /\t/
syntax match ExtraSpaces /\s\+$/
"""" Setup Colorscheme
set background=dark " fix to make vim realize we are actually in a dark terminal
colorscheme zenburn
"""" Color schemes for custom hilights
autocmd ColorScheme * hi ExtraSpaces ctermbg=darkred guibg=darkred ctermfg=darkred guifg=darkred
autocmd ColorScheme * hi Tabs ctermbg=darkred guibg=darkred ctermfg=darkred guifg=darkred
autocmd ColorScheme * hi SpacesBeforeTabs ctermbg=darkred guibg=darkred ctermfg=darkred guifg=darkred
hi Search ctermbg=White ctermfg=Black
hi CursorLine ctermbg=240
hi CursorColumn ctermbg=240
autocmd InsertLeave * redraw! " redraw on insert leave
"""" One shade darker background for lines over 80 characters
hi ColorColumn ctermbg=233
execute "set colorcolumn=" . join(range(81,355), ',')
"""" lines over 120 characters long
let w:m2=matchadd('ErrorMsg', '\%>120v.\+', -1)
au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>120v.\+', -1)

" === Keybindings ===
let mapleader = ","
set pastetoggle=<F2>

"""" Move to end of line with L and beginning of line with H
nnoremap H 0
nnoremap L $

"""" YouCompleteMe lifesavers
" *g*oTo
nnoremap <leader>g :YcmCompleter GoTo<CR>
" *r*e*c*ompile
nnoremap <leader>rc :YcmForceCompileAndDiagnostics<CR>

"""" Remove hilights ,<space>
nnoremap <leader><space> :noh<cr>

"""" Find cursor ,c
nnoremap <leader>c :set cursorline! cursorcolumn!<CR>

"""" Quicksave ,s
map <leader>s :w<CR>
vmap <leader>s <C-C>:w<CR>
imap <leader>s <C-O>:w<CR>

"""" Quick movement between windows ctrl+j/k/l/h
noremap <c-j> <c-w>j
noremap <c-k> <c-w>k
noremap <c-l> <c-w>l
noremap <c-h> <c-w>h

"""" Quick splitting
noremap <leader><bar> <esc>:vsplit<CR>
noremap <leader>- <esc>:split<CR>

"""" Quick movement between buffers ,h/l
noremap <leader>h <esc>:bprevious<CR>
noremap <leader>l <esc>:bnext<CR>

"""" buffers ,tc Creates; ,tq Quits
noremap <leader>tc <esc>:enew<CR>
noremap <leader>tq <esc>:bp <BAR> bd #<CR>

"""" List buffers
noremap <leader>ls <esc>:ls<CR>

"""" Easier indenting (keeps the visual line after indent)
vnoremap < <gv
vnoremap > >gv

"""" Easier lineswapping ,+<up arrow>/<down arrow>
noremap <leader><up> :m .-2<CR>
noremap <leader><down> :m .+1<CR>

"""" Remove trailing spaces F3
nnoremap <silent> <F3> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

" === Editing configs ===
"""" Misc
set backspace=indent,eol,start
"""" Remove backup and swap files
set noswapfile
set nobackup
set nowb
"""" How files are written
set noeol
set enc=utf-8
set fileencoding=utf-8
"""" Indents
set expandtab " tabs to spaces
set tabstop=4 " tab is width in columns
set shiftwidth=4 " when shifting lines, how many columns
set softtabstop=4 " number of spaces a tab is while editing
set cindent
set shiftround " round indent to a multiple of shiftwidth
set autoindent
"autocmd FileType c,cpp setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType python setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd FileType yaml setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType go setlocal tabstop=4 shiftwidth=4 softtabstop=4
"""" Linelengths
set tw=120
autocmd BufRead *.txt,*.md set tw=72
" q = allow formatting of comments with gq
" r = automatically insert the current comment leader after hitting <enter> in insert mode
" n = recognize numbered lists
" j = remove comment leader when joining two comment lines
" 1 = don't break a line after a one-letter word
set formatoptions=qrnj1
"""" Search
set ignorecase
set smartcase
set gdefault " g in s///g is always on
set incsearch " highlight search while writing
set showmatch
"""" Correct idiotic typos in commands
cnoreabbrev W w
cnoreabbrev Wq wq
cnoreabbrev Q q
cnoreabbrev Qa qa
cnoreabbrev wQ wq
cnoreabbrev Wqa wqa

if ! has('gui_running')
    set ttimeoutlen=10
    augroup FastEscape
        autocmd!
        au InsertEnter * set timeoutlen=0
        au InsertLeave * set timeoutlen=1000
    augroup END
endif

" ======================================================================
" Plugin related stuff, also very awesome!
" ======================================================================
" airline
let g:airline_theme="bubblegum"
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ctrlp#show_adjacent_modes = 1
let g:airline#extensions#tabline#fnamemod = ':t'

" Flake8
let g:PyFlakeMaxLineLength=120
let g:PyFlakeForcePyVersion=3

" YCM / Ultisnips
set completeopt=menuone,longest,preview
let g:ycm_always_populate_location_list = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_confirm_extra_conf = 0
let g:ycm_max_diagnostics_to_display = 5
let g:ycm_min_num_of_chars_for_completion = 3
let g:ycm_open_loclist_on_ycm_diags = 1
let g:ycm_seed_identifiers_with_syntax = 1

" Ctrl-P
let g:ctrlp_map = '<leader>/'
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_extensions = ['tag', 'mixed']
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_follow_symlinks = 1
let g:ctrlp_cache_dir = $HOME.'/.cache/ctrlp'
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
                                \ --ignore .git --ignore .svn
                                \ --ignore "**/*.pyc" -g ""'
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }

" Easytags
set tags=./.tags;
let g:easytags_async = 1
let g:easytags_dynamic_files = 2
let g:easytags_always_enabled = 1
"" Hilighting is suuuuper slow
let g:easytags_syntax_keyword = 'always'

" Isort
autocmd BufWritePre *.py Isort

let g:loaded_python_provider = 1
